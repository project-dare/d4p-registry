dj-vercereg
===========

*dj-vercereg* is the [dispel4py](https://github.com/akrause2014/dispel4py) Registry reference implementation in [Django](https://www.djangoproject.com). The dispel4py registry was originally developed as part of the [VERCE](http://verce.eu) project (FP7-INFRASTRUCTURES-2011-2, Project no. 283543).

The dispel4py Registry is a RESTful Web service providing functionality for registering workflow entities, such as processing elements (PEs), functions and literals, while encouraging sharing and collaboration via groups and workspaces. More information can be found in 

* I. A. Klampanos, P. Martin and M. Atkinson (2019, August 6). Consistency and Collaboration for Fine-Grained Scientific Workflow Development: The dispel4py Information Registry. Zenodo. http://doi.org/10.5281/zenodo.3361395
