# Copyright 2014 The University of Edinburgh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import json
import logging

import requests
from django.contrib.auth.models import User
from requests.exceptions import ConnectionError, ConnectTimeout
from rest_framework import permissions

from dj_vercereg.settings import DARE_LOGIN_URL

logger = logging.getLogger(__name__)


class IsAuthenticated(permissions.IsAuthenticated):
    """
    Class that is used to implement the has_permission method. Extends the Django authentication class in order to
    implement external authentication from dare-login module.
    """

    def has_permission(self, request, view):
        """
        This method is called in the views that include the IsAuthenticated class in permissions. Performs a RESTful
        API call to the dare-login in order to authenticate the user.

        Args
            | request (HttpRequest): django Http request object containing all the provided information by the client.
            | view (ModelViewSet): the specific view that triggered the method.

        Returns
            bool: True/False, whether the user is successfully authenticated to the platform
        """
        if "docs" in request.build_absolute_uri():
            return True
        if type(request.data) != dict and not request.data._mutable:
            request.data._mutable = True
        if type(request.query_params) != dict and not request.query_params._mutable:
            request.query_params._mutable = True
        if request.method == "POST" or request.method == "PUT" or request.method == "DELETE":
            token = request.data["access_token"]
        else:
            token = request.query_params["access_token"]
        if token:
            try:
                user_data = self.validate_token(token=token)
                if user_data[0] == 200:
                    user_data = json.loads(user_data[1])
                    user = User.objects.get(username=user_data["username"])
                    user = self.get_user(user.id)
                    request.user = user
                    if request.method == "POST" or request.method == "PUT" or request.method == "DELETE":
                        request.data["username"] = user_data["username"]
                        request.data["user_id"] = user_data["user_id"]
                        request.data["issuer"] = user_data["issuer"]
                    else:
                        request.query_params["username"] = user_data["username"]
                        request.query_params["user_id"] = user_data["user_id"]
                        request.query_params["issuer"] = user_data["issuer"]
                    return True
                else:
                    return False
            except (ConnectionError, ConnectTimeout, Exception) as e:
                logger.error("Could not authenticate user {}".format(e))
        else:
            logger.error("Token was not provided in the request")
            return False

    @staticmethod
    def get_user(id_):
        try:
            return User.objects.get(pk=id_)  # <-- tried to get by email here
        except User.DoesNotExist:
            return None

    @staticmethod
    def validate_token(token):
        """
        Function to validate a given token. If the token is valid, the API asks for user info so as to retrieve a
        permanent id. Some of the returned fields (if token is valid) are as presented below:

        - "sub": "cfbdc618-1d1e-4b1d-9d35-3d23f271abe3",
        - "email_verified": true,
        - "name": "Sissy Themeli",
        - "preferred_username": "sthemeli",
        - "given_name": "Sissy",
        - "family_name": "Themeli",
        - "email": "sthemeli@iit.demokritos.gr"

        Args
            request: http request
            oidc: open-id client

        Returns
            str: user's permanent id
        """
        try:
            url = "{}/validate-token".format(DARE_LOGIN_URL)
            response = requests.post(url, data=json.dumps({"access_token": token}))
            return response.status_code, response.text
        except (ConnectionError, Exception) as e:
            print(e)
            return 500, "Could not validate token"


class RegistryUserGroupAccessPermissions(permissions.BasePermission):
    """Defines permissions for accessing REST registryusergroup objects"""

    def has_permission(self, request, view):
        """
        General DELETE permissions given only to staff and superusers;
        other methods are granted to everyone.
        TODO: Check this works as expected.
        """
        if request.method == 'DELETE':
            return request.user.is_superuser or request.user.is_staff
        else:
            return True

    def has_object_permission(self, request, view, obj):
        """Full permissions for superusers, staff or group owners"""
        if request.method not in permissions.SAFE_METHODS:
            return (request.user.is_superuser or
                    request.user.is_staff or
                    request.user == obj.owner)
        else:
            return True


class ConnectionPermissions(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return (request.user.is_superuser or
                    request.user.is_staff or
                    request.user == obj.pesig.user or
                    len(request.user.groups.filter(
                        name='default_read_all_group')) > 0)
        else:
            return (request.user.is_superuser or
                    request.user.is_staff or
                    request.user == obj.pesig.user)


class FunctionParameterPermissions(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return (request.user.is_superuser or
                    request.user.is_staff or
                    request.user == obj.parent_function.user or
                    len(request.user.groups.filter(
                        name='default_read_all_group')) > 0)
        else:
            return (request.user.is_superuser or
                    request.user.is_staff or
                    request.user == obj.parent_function.user)


class UserAccessPermissions(permissions.BasePermission):
    """Defines permissions for accessing REST user objects"""

    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            return request.user.is_superuser or request.user.is_staff
        else:
            return True

    def has_object_permission(self, request, view, obj):
        if request.method == 'PUT':
            return (request.user.is_superuser or
                    request.user.is_staff or
                    request.user == obj)
        elif request.method in permissions.SAFE_METHODS:
            return True
            # (request.user.is_superuser or
            # request.user.is_staff or
            # request.user == obj)
        else:
            return (request.user.is_superuser or
                    request.user.is_staff)


class WorkspaceItemPermissions(permissions.BasePermission):
    """
    Handles the permissions of workspace items.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            if 'copy_to' in request.query_params:
                return (request.user == obj.workspace.owner or
                        request.user.is_superuser or
                        request.user.is_staff or
                        request.user.has_perm(
                            'modify_contents_workspace', obj.workspace))
            else:
                return (request.user == obj.user or
                        request.user.is_superuser or
                        request.user.is_staff or
                        len(request.user.groups.filter(
                            name='default_read_all_group')) > 0)
        else:
            return (request.user == obj.workspace.owner or
                    request.user.is_superuser or
                    request.user.is_staff or
                    request.user.has_perm(
                        'modify_contents_workspace', obj.workspace))


class WorkspaceBasedPermissions(permissions.BasePermission):
    """
    Handles permissions for workspaces.
    TODO: Document permissions properly...
    """

    # def has_permission(self, request, view):
    #   return True

    def has_object_permission(self, request, view, obj):
        is_owner = request.user == obj.owner
        is_superuser = request.user.is_superuser

        if request.method in permissions.SAFE_METHODS:
            has_view_meta_perms = request.user.has_perm(
                'view_meta_workspace',
                obj)
            return has_view_meta_perms or is_owner or is_superuser or len(
                request.user.groups.filter(
                    name='default_read_all_group')) > 0
        else:
            if request.method == 'PUT':
                has_modify_meta_perms = request.user.has_perm(
                    'modify_meta_workspace',
                    obj)
                return has_modify_meta_perms or is_superuser or is_owner
            elif request.method == 'POST':
                has_view_contents = request.user.has_perm(
                    'view_contents_workspace',
                    obj)
                return has_view_contents or is_superuser or is_owner
            elif request.method == 'DELETE':
                return is_superuser
            else:
                return False
